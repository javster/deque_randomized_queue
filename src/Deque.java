import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by anton.kuritsyn on 12.10.2016.
 */
public class Deque<Item> implements Iterable<Item> {

    private class Node  {
        private Node next;
        private Node prev;
        private Item item;

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node prev) {
            this.prev = prev;
        }

        public Item getItem() {
            return item;
        }

        public void setItem(Item item) {
            this.item = item;
        }
    }

    private Node first;
    private Node last;
    private int count;

    @Override
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public int size() {
        return count;
    }

    public void addFirst(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }

        Node oldFirst = first;

        first = new Node();
        first.setItem(item);

        if (oldFirst != null) {
            first.setNext(oldFirst);
            oldFirst.setPrev(first);
        } else {
            last = first;
        }
        count++;
    }

    public void addLast(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }

        Node oldLast = last;

        last = new Node();
        last.setItem(item);

        if (oldLast != null) {
            last.setPrev(oldLast);
            oldLast.setNext(last);
        } else {
            first = last;
        }
        count++;
    }

    public Item removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        Item result = first.getItem();
        first = first.getNext();
        if (first == null) {
            last = null;
        }
        count--;
        return result;
    }

    public Item removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        Item result = last.getItem();
        last = last.getPrev();
        if (last == null) {
            first = null;
        }
        count--;
        return result;
    }

    private class DequeIterator implements Iterator<Item> {

        private Node current;

        @Override
        public boolean hasNext() {
            return current != last;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            if (current == null) {
                current = first;
            } else {
                current = current.getNext();
            }

            Item item = current.getItem();

            return item;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static void main(String[] args) {
    }
}
