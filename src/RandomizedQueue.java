import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by anton.kuritsyn on 12.10.2016.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {

    private static final int INITIAL_SIZE = 10;

    private Item[] items;
    private int head;
    private int removedCount;

    public RandomizedQueue() {
        items = (Item[]) new Object[INITIAL_SIZE];
    }

    public boolean isEmpty() {
        return head == 0 || head == removedCount;
    }

    public int size() {
        return head - removedCount;
    }

    private int arraySize() {
        return items.length;
    }

    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        items[head++] = item;
        if (head == items.length) {
            changeSize(items.length * 2 - removedCount);
        }
    }

    public Item dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        int i;
        do {
            i = StdRandom.uniform(head);
        } while (items[i] == null);

        Item item = items[i];
        items[i] = null;

        removedCount++;
        if (removedCount >= (items.length + items.length * 0.5) / 2) {
            changeSize(items.length / 2);
        }
        return item;
    }

    public Item sample() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        int i;
        do {
            i = StdRandom.uniform(head);
        } while (items[i] == null);

        return items[i];
    }

    private void changeSize(int newSize) {
        Item[] oldArray = items;
        items = (Item[]) new Object[newSize];
        head = 0;
        for (int i = 0; i < oldArray.length; i++) {
            if (oldArray[i] != null) {
                items[head++] = oldArray[i];
            }
        }
        removedCount = 0;
    }

    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {

        private Item[] shuffledItems;
        private int counter;

        @Override
        public boolean hasNext() {
            return counter < shuffledItems.length;
        }

        public RandomizedQueueIterator() {
            shuffledItems = (Item[])new Object[size()];
            int j = 0;
            for (int i = 0; i < items.length; i++) {
                if (items[i] != null) {
                    shuffledItems[j++] = items[i];
                }
            }
            StdRandom.shuffle(shuffledItems);
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return shuffledItems[counter++];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }


    public static void main(String[] args) {
    }
}