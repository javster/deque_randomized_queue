import edu.princeton.cs.algs4.StdIn;

import java.util.Iterator;

/**
 * Created by anton.kuritsyn on 15.10.2016.
 */
public class Subset {

    public static void main(String[] args) {
        if (args.length < 1) {
            return;
        }

        int k = Integer.parseInt(args[0]);

        if (k < 0) {
            return;
        }

        RandomizedQueue<String> randomizedQueue = new RandomizedQueue<>();

        while(!StdIn.isEmpty()) {
            randomizedQueue.enqueue(StdIn.readString());
        }

        Iterator<String> it = randomizedQueue.iterator();
        for (int i = 0; i < k; i++) {
            if (it.hasNext()) {
                System.out.println(it.next());
            }
        }
    }
}